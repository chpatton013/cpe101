#include <stdio.h>
#include <assert.h>
#include <math.h>

#define INIT 3
#define MAX_SHAPES 100
#define MAX_PIXELS 200

#define CIRCLE_ARGS 5
#define RECT_ARGS 6
#define ELLIPSE_ARGS 7

#define BLACK 0
#define RED 1
#define GREEN 2
#define BLUE 3
#define CYAN 4
#define MAGENTA 5
#define YELLOW 6
#define WHITE 7

typedef struct {
   char type;
   int color, xoff, yoff, arg0, arg1, arg2, args;
} Shape;

int CheckArgs(Shape shape, int numArgs, int init[3]);
int CheckRectangle(int numArgs, Shape shape);
int CheckCircle(int numArgs, Shape shape);
int CheckEllipse(int numArgs, Shape shape);
int CheckExclaim(int numArgs, Shape shape);

int InObject(char type, int x, int y, int arg0, int arg1, int arg2);
int Rectangle(int width, int height, int x, int y);
int Circle(double radius, double x, double y);
int Ellipse(int fX, int fY, int axis, int x, int y);
int Exclaim(int width, int height, int x, int y);

double dist(int x1, int x2, int y1, int y2);
void printImage(int init[INIT], int pixel[MAX_PIXELS][MAX_PIXELS]);

int main(void)
{
   int init[INIT], pixel[MAX_PIXELS][MAX_PIXELS], i, j, k, numArgs;
   Shape shape[MAX_SHAPES];
   
   scanf("%d %d %d", &init[0], &init[1], &init[2]);
   
   for (i = 0; i < MAX_SHAPES; i++) {
      
      numArgs = scanf(" %c %d %d %d %d %d %d", &shape[i].type,
       &shape[i].color, &shape[i].xoff, &shape[i].yoff,
       &shape[i].arg0, &shape[i].arg1, &shape[i].arg2);
      
      if (numArgs == -1 && i < init[0]) {
         printf("Must have %d objects.", i);
         return 0;
      }
      
      else if (numArgs != -1 && i > init[0] - 1) {
         printf("Must have %d objects.", i + 1);
         return 0;
      }
      
      shape[i].args = numArgs;
   }
   
   if (init[1] < 0 || init[1] > MAX_PIXELS ||
    init[2] < 0 || init[2] > MAX_PIXELS) {
      printf("Must have positive dimensions at most %d.", MAX_PIXELS);
      return 0;
   }
   
   for (i = 0; i < init[0]; i++) {
      if (! CheckArgs(shape[i], shape[i].args, init)) {
         printf("Error in object %d.", i+1);
         return 0;
      }
   }
   
   for (i = 0; i < init[2]; i++) {
      for (j = 0; j < init[1]; j++) {
         pixel[i][j] = BLACK;
         
         for (k = init[0] - 1; k >= 0; k--) { 
            if (InObject(shape[k].type, j - shape[k].xoff, i - shape[k].yoff,
             shape[k].arg0, shape[k].arg1, shape[k].arg2) == 1)
               pixel[i][j] = shape[k].color;
         }
      }
   }
   
   printImage(init, pixel);
   
   return 0;
}

int CheckArgs(Shape shape, int numArgs, int init[3])
{
   if (shape.color < 0 || shape.color > 7) {
      printf("Invalid color.\n");
      return 0;
   }
   
   if (shape.type == 'R')
      return CheckRectangle(numArgs, shape);
   
   else if (shape.type == 'E')
      return CheckEllipse(numArgs, shape);
   
   else if (shape.type == 'C')
      return CheckCircle(numArgs, shape);
   
   else if (shape.type == '!')
      return CheckExclaim(numArgs, shape);
   
   else {
      printf("Bad shape %c.\n", shape.type);
      return 0;
   }
}

int CheckRectangle(int numArgs, Shape shape)
{
   if (numArgs != RECT_ARGS) {
      printf("Rectangle must have six arguments.\n");
      return 0;
   }

   if (shape.arg0 < 0 || shape.arg1 < 0) {
      printf("Rectangle must have positive width and height.\n");
      return 0;
   }
   
   return 1;
}

int CheckCircle(int numArgs, Shape shape)
{
   if (numArgs != CIRCLE_ARGS) {
      printf("Circle must have five arguments.\n");
      return 0;
   }

   if (shape.arg0 < 0) {
      printf("Circle must have positive radius.\n");
      return 0;
   }
   
   return 1;
}

int CheckEllipse(int numArgs, Shape shape)
{
   if (numArgs != ELLIPSE_ARGS) {
      printf("Ellipse must have seven arguments.\n");
      return 0;
   }

   if (shape.arg2 < 0) {
      printf("Ellipse must have positive axis.\n");
      return 0;
   }
   
   return 1;
}

int CheckExclaim(int numArgs, Shape shape)
{
   if (numArgs != RECT_ARGS) {
      printf("Exclamation must have six arguments.\n");
      return 0;
   }

   if (shape.arg0 < 0 || shape.arg1 < 0) {
      printf("Exclamation must have positive width and height.\n");
      return 0;
   }
   
   return 1;
}

int InObject(char type, int x, int y, int arg0, int arg1, int arg2)
{
   if (type == 'R')
      return Rectangle(arg0, arg1, x, y);
   
   else if (type == 'C')
      return Circle(arg0, x, y);
   
   else if (type == 'E')
      return Ellipse(arg0, arg1, arg2, x, y);
   
   else if (type == '!')
      return Exclaim(arg0, arg1, x, y);
   
   else
      return 0;
}

int Rectangle(int width, int height, int x, int y)
{
   return !(x < 0 || y < 0 || x >= width || y >= height);
}

int Circle(double radius, double x, double y)
{
   return (dist(0, x, 0, y) < radius);
}

int Ellipse(int fX, int fY, int axis, int x, int y)
{
   return ((dist (0, x, 0, y) + dist (fX, x, fY, y)) < axis);
}

int Exclaim(int width, int height, int x, int y)
{
   return (Rectangle(width, height, x, y) ||
    Rectangle(width, width, x, y - height - width / 2));
}

double dist(int x1, int x2, int y1, int y2)
{
   return sqrt(pow (x2 - x1, 2) + pow(y2 - y1, 2));
}

void printImage(int init[3], int pixel[MAX_PIXELS][MAX_PIXELS])
{
   int i, j;
   
   printf("P3 %d %d 255\n", init[1], init[2]);
   
   for (i = 0; i < init[2]; i++) {
      for (j = 0; j < init[1]; j++) {
         
         if (pixel[i][j] == BLACK)
            printf("0 0 0");
         
         else if (pixel[i][j] == RED)
            printf("255 0 0");
         
         else if (pixel[i][j] == GREEN)
            printf("0 255 0");
         
         else if (pixel[i][j] == BLUE)
            printf("0 0 255");
         
         else if (pixel[i][j] == CYAN)
            printf("0 255 255");
         
         else if (pixel[i][j] == MAGENTA)
            printf("255 0 255");
         
         else if (pixel[i][j] == YELLOW)
            printf("255 255 0");
         
         else if (pixel[i][j] == WHITE)
            printf("255 255 255");
         
         printf(" # %d,%d\n", j, i);
      }
   }
}
