#include <stdio.h>
#include <assert.h>
#include <math.h>

#define INIT 3
#define MAX 200
#define BLACK 0
#define RED 1
#define GREEN 2
#define BLUE 3
#define CYAN 4
#define MAGENTA 5
#define YELLOW 6
#define WHITE 7

int Rectangle(int width, int height, int x, int y);
int Ellipse(int fX, int fY, int axis, int x, int y);
int Circle(double radius, double x, double y);
int InObject(char type, int x, int y, int arg1, int arg2, int arg3);
double dist(int x1, int x2, int y1, int y2);
void printImage(int init[INIT], int pixel[MAX][MAX]);

typedef struct {
   char type;
   int color, xoff, yoff, arg1, arg2, arg3;
} Shape;

int main (void)
{
   int init[INIT], pixel[MAX][MAX], i, j, k;
   Shape shape[MAX];
   
   scanf("%d %d %d", &init[0], &init[1], &init[2]);
   
   for (i = 0; i < init[0]; i++) {
      scanf(" %c %d %d %d %d %d %d", &shape[i].type,
       &shape[i].color, &shape[i].xoff, &shape[i].yoff,
       &shape[i].arg1, &shape[i].arg2, &shape[i].arg3);
   }
   
   for (i = 0; i < init[2]; i++) {
      for ( j = 0; j < init[1]; j++) {
         pixel[i][j] = BLACK;
         
         for (k = init[0] - 1; k >= 0; k--) { 
            if(InObject(shape[k].type, j - shape[k].xoff, i-shape[k].yoff,
             shape[k].arg1, shape[k].arg2, shape[k].arg3) == 1)
               pixel[i][j] = shape[k].color;
         }
      }
   }
   
   printImage(init, pixel);
   
   return 0;
}

int Rectangle(int width, int height, int x, int y)
{
   if (x < 0 || y < 0 || x >= width || y >= height)
      return 0;
   return 1;
}

int Ellipse(int fX, int fY, int axis, int x, int y)
{
   if (! ((dist(0, x, 0, y) + dist(fX, x, fY, y)) < axis))
      return 0;
   return 1;
}

int Circle(double radius, double x, double y)
{
   if (! ((dist(0, x, 0, y)) < radius))
      return 0;
   return 1;
}

int InObject(char type, int x, int y, int arg1, int arg2, int arg3)
{
   if (type == 'R')
      return Rectangle(arg1, arg2, x, y);
   else if (type == 'C')
      return Circle(arg1, x, y);
   else if (type == 'E')
      return Ellipse(arg1, arg2, arg3, x, y);
   else
      return 0;
}

double dist(int x1, int x2, int y1, int y2)
{
   return sqrt(pow (x2 - x1, 2) + pow(y2 - y1, 2));
}

void printImage(int init[INIT], int pixel[MAX][MAX])
{
   int i, j;
   
   printf("P3 %d %d 255\n", init[1], init[2]);
   
   for (i = 0; i < init[2]; i++) {
      for (j = 0; j < init[1]; j++) {
         if (pixel[i][j] == BLACK) printf("0 0 0");
         else if (pixel[i][j] == RED) printf("255 0 0");
         else if (pixel[i][j] == GREEN) printf("0 255 0");
         else if (pixel[i][j] == BLUE) printf("0 0 255");
         else if (pixel[i][j] == CYAN) printf("0 255 255");
         else if (pixel[i][j] == MAGENTA) printf("255 0 255");
         else if (pixel[i][j] == YELLOW) printf("255 255 0");
         else if (pixel[i][j] == WHITE) printf("255 255 255");
         printf(" # %d,%d\n", j, i);
      }
   }
}
