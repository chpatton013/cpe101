/*                               80 col header                                */
#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "Point.h"

Point DiffPoint(Point p, int xOffs, int yOffs)
{
   p.x -= xOffs;
   p.y -= yOffs;
   return p;
}

double Distance(Point p1, Point p2)
{
   return sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
}
