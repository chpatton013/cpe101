#ifndef COLOR_H
#define COLOR_H

#include "Point.h"

typedef struct {
   int red;
   int green;
   int blue;
} Color;

void DisplayColor(Color clr, Point loc);
int IsGoodColor(Color clr);

#endif
