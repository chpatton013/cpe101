/*                               80 col header                                */
#include <stdio.h>
#include <assert.h>

#include "Color.h"
#include "Point.h"

void DisplayColor(Color clr, Point loc)
{
   printf("%d %d %d # %d,%d\n", clr.red, clr.green, clr.blue, loc.x, loc.y);
}

int IsGoodColor(Color clr)
{
   return !(clr.red < 0 || clr.red > 255 || clr.green < 0 ||
    clr.green > 255 || clr.blue < 0 || clr.blue > 255);
}
