#ifndef SHAPE_H
#define SHAPE_H

#include "Color.h"
#include "Point.h"

/* A struct representing a single shape, incluDing its type, color,
 and shape-specific arguments.  It doesn't include x/y location; these are
 maintained elsewhere. */

typedef struct {
   char type;
   Color color;
   int arg0;
   int arg1;
   int arg2;
} Shape;

// Return true if point "p" is within rectangle "s", assuming s is based at 
// the origin. It's an error to call this method on any but a rectangle shape,
// and if you want a rectangle not based at the origin, you need to adjust p
// accordingly, before you call Rectangle
int Rectangle(Shape s, Point p);

// Checks a rectangle shape for correct configuration, including positive
// dimensions. Checks first that the correct number of arguments was given 
// in input.  Prints appropriate error messages if problems are found.  
// Returns  true iff all is well.
int CheckRectangle(Shape s, int numArgs);

// Like Rectangle, but for ellipses
int Ellipse(Shape s, Point p);

// Like CheckRectangle, but for ellipses
int CheckEllipse(Shape s, int numArgs);

// Like Rectangle, but for circles
int Circle(Shape s, Point p);

// Like CheckRectangle, but for circles
int CheckCircle(Shape s, int numArgs);

// Return true if point "p" is within H-shape "s", assuming s is based at 
// the origin. It's an error to call this method on any but an H-shape, and
// if you want an H-shape not based at the origin, you need to adjust p
// accordingly, before you call LetterH.
int LetterH(Shape s, Point p);

// Like LetterH, but for L-shapes
int LetterL(Shape s, Point p);

// Like LetterH but for exclamation points.
int Exclaim(Shape s, Point p);

// Checks any H, or L-shape for correct configuration, including
// positive dimensions and a stroke at most 1/4 of the dimensions.  Checks
// first that the correct number of arguments were given in input.  Prints
// appropriate error messages if problems are found.  Returns  true iff all 
// is well.
int CheckLetter(Shape s, int numArgs);

// Like CheckLetter but for exclamation shapes
int CheckExclaim(Shape s, int numArgs);

#endif
