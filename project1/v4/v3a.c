/*                               80 col header                                */
#include <stdio.h>
#include <assert.h>
#include <math.h>

#define SHAPES 100
#define MAX_SIZE 200

#define INIT_ARGS 3
#define MIN_ARGS 4
#define RECT_ARGS 6
#define ELPS_ARGS 7
#define CIRC_ARGS 5
#define EXCL_ARGS 6
#define LETR_ARGS 7
#define STROKE_FACTOR 4

#define BLACK 0
#define RED 1
#define GREEN 2
#define BLUE 3
#define CYAN 4
#define MAGENTA 5
#define YELLOW 6
#define WHITE 7

int InObject(char type, int x, int y, int arg0, int arg1, int arg2);
int CheckArgs(int type, int color, int arg0, int arg1, int arg2,
 int numArgs);

int Rectangle(int x, int y, int arg0, int arg1);
int CheckRectangle(int arg0, int arg1, int numArgs);

int Ellipse(int x, int y, int arg0, int arg1, int arg2);
int CheckEllipse(int arg2, int numArgs);

int Circle(int x, int y, int arg0);
int CheckCircle(int arg0, int numArgs);

int Exclaim(int x, int y, int arg0, int arg1);
int CheckExclaim(int arg0, int arg1, int numArgs);

int LetterH(int x, int y, int arg0, int arg1, int arg2);
int LetterL(int x, int y, int arg0, int arg1, int arg2);
int CheckLetter(char type, int arg0, int arg1, int arg2, int numArgs);

void DisplayColor(int clr, int x, int y);
int IsGoodColor(int clr);

int DiffPoint(int x, int offs);
double Distance(int x1, int y1, int x2, int y2);

int main(void)
{
   int numArgs, numShapes, imgW, imgH, i, j, k, layer = -1;
   char type[SHAPES];
   int color[SHAPES], x[SHAPES], y[SHAPES];
   int arg0[SHAPES], arg1[SHAPES], arg2[SHAPES];
   
   numArgs = scanf("%d %d %d", &numShapes, &imgW, &imgH);
   
   if (numArgs == EOF) {
      printf ("Empty input file.\n");
      return 0;
   }
   
   else if (numArgs != INIT_ARGS) {
      printf("Initial conditions missing.\n");
      return 0;
   }
   
   if (numShapes < 0 || numShapes > SHAPES) {
      printf("Must have positive number of objects at most %d.\n", SHAPES);
      return 0;
   }
   
   if (imgW < 0 || imgW > MAX_SIZE || imgH < 0 || imgH > MAX_SIZE) {
      printf("Must have positive dimensions at most %d.\n", MAX_SIZE);
      return 0;
   }
   
   for (i = 0; i < numShapes; i++) {
      numArgs = scanf(" %c %d %d %d %d %d %d", &type[i], &color[i],
       &x[i], &y[i], &arg0[i], &arg1[i], &arg2[i]);
      
      if (numArgs == EOF) {
         printf("Must have %d shapes.\n", numShapes);
         return 0;
      }
      
      if (!CheckArgs(type[i], color[i], arg0[i], arg1[i], arg2[i], numArgs)) {
         printf("Error in object %d.\n", i);
         return 0;
      }
   }
   
   numArgs = scanf(" %c %d %d %d %d %d %d", &type[i], &color[i],             
    &x[i], &y[i], &arg0[i], &arg1[i], &arg2[i]);
   
   if (numArgs != EOF) {
      printf("Must have %d shapes.\n", numShapes);
      return 0;
   }
   
   printf("P3 %d %d 255\n", imgW, imgH);
   
   for (i = 0; i < imgH; i++) {
      for (j = 0; j < imgW; j++) {
         
         for (k = numShapes-1; k >= 0; k--)
            if (InObject(type[k], i - x[k], j - y[k],
             arg0[k], arg1[k], arg2[k]))
               layer = k;
         
         if (layer != -1)
            DisplayColor(color[layer], i, j);
         else
            DisplayColor(0, i, j);
         
         layer = -1;
      }
   }
   
   return 0;
}

int InObject(char type, int x, int y, int arg0, int arg1, int arg2)
{
   if (type == 'R')
      return Rectangle(x, y, arg0, arg1);
   else if (type == 'C')
      return Circle(x, y, arg0);
   else if (type == 'E')
      return Ellipse(x, y, arg0, arg1, arg2);
   else if (type == '!')
      return Exclaim(x, y, arg0, arg1);
   else if (type == 'H')
      return LetterH(x, y, arg0, arg1, arg2);
   else if (type == 'L')
      return LetterL(x, y, arg0, arg1, arg2);
   else
      return 0;
}

int CheckArgs(int type, int color, int arg0, int arg1, int arg2,
 int numArgs)
{
   if (numArgs < MIN_ARGS) {
      printf("Bad formatting in first %d args.\n", MIN_ARGS);
      return 0;
   }
   
   if (!IsGoodColor(color)) {
      printf("Bad color.\n");
      return 0;
   }
   
   if (type == 'R')
      return CheckRectangle(arg0, arg1, numArgs);
   else if (type == 'E')
      return CheckEllipse(arg2, numArgs);
   else if (type == 'C')
      return CheckCircle(arg0, numArgs);
   else if (type == '!')
      return CheckExclaim(arg0, arg1, numArgs);
   else if (type == 'H' || type == 'L')
      return CheckLetter(type, arg0, arg1, arg2, numArgs);
   else {
      printf("Bad shape %c.\n", type);
      return 0;
   }
   
   return 1;
}

int Rectangle(int x, int y, int arg0, int arg1)
{
   return !(x < 0 || x >= arg0 || y < 0 || y >= arg1);
}

int CheckRectangle(int arg0, int arg1, int numArgs)
{
   if (numArgs != RECT_ARGS) {
      printf("Rectangle must have exactly two arguments.\n");
      return 0;
   }
   
   if (arg0 < 0 || arg1 < 0) {
      printf("Rectangle must have positive width and height.\n");
      return 0;
   }
   
   return 1;
}

int Ellipse(int x, int y, int arg0, int arg1, int arg2)
{
   return ((Distance(x, 0, y, 0) + Distance(arg0, 0, arg1, 0)) < arg2);
}

int CheckEllipse(int arg2, int numArgs)
{
   if (numArgs != ELPS_ARGS) {
      printf("Ellipse must have exactly three arguments.\n");
      return 0;
   }
   
   if (arg2 < 0) {
      printf("Ellipse must have positive axis.\n");
      return 0;
   }
   
   return 1;
}

int Circle(int x, int y, int arg0)
{
   return (Distance(x, 0, y, 0) < arg0);
}

int CheckCircle(int arg0, int numArgs)
{
   if (numArgs != CIRC_ARGS) {
      printf("Circle must have exactly one argument.\n");
      return 0;
   }
   
   if (arg0 < 0) {
      printf("Circle must have positive radius.\n");
      return 0;
   }
   
   return 1;
}

int Exclaim(int x, int y, int arg0, int arg1)
{
   return (Rectangle(x, y, arg0, arg1) ||
    Rectangle(x, y - arg1 - arg0 / 2, arg0, arg0));
}

int CheckExclaim(int arg0, int arg1, int numArgs)
{
   if (numArgs != EXCL_ARGS) {
      printf("Exclamation must have exactly two arguments.\n");
      return 0;
   }
   
   if (arg0 < 0 || arg1 < 0) {
      printf("Exclamation must have positive width and height.\n");
      return 0;
   }
   
   return 1;
}

int LetterH(int x, int y, int arg0, int arg1, int arg2)
{
/*   Shape leftS, rightS, midS;
   Point rightP = {s.arg0 - s.arg2, 0}, midP = {0, s.arg1 / 2};
   
   leftS.arg0 = s.arg2;
   rightS.arg0 = s.arg2;
   midS.arg1 = s.arg2;
  */ 
   if (Rectangle(x, y, arg2, arg1) ||
    Rectangle(x + arg1 - arg2, y, arg2, arg1) ||
    Rectangle(x, y + arg2 / 2, arg0, arg2))
      return 1;
   return 0;
}

int LetterL(int x, int y, int arg0, int arg1, int arg2)
{
   return (Rectangle(x, y, arg2, arg1) ||
    Rectangle(x, y + arg1 - arg2, arg0, arg2));
}

int CheckLetter(char type, int arg0, int arg1, int arg2, int numArgs)
{
   if (numArgs != LETR_ARGS) {
      printf("Letters must have exactly three arguments.\n");
      return 0;
   }
   
   if (arg0 < 0 || arg1 < 0) {
      printf("Letters must have positive width and height.\n");
      return 0;
   }
   
   if (arg2 * STROKE_FACTOR > arg0 || arg2 * STROKE_FACTOR > arg1) {
      printf("Letter %c must have width and height at least %d*stroke.\n",
       type, STROKE_FACTOR);
      return 0;
   }
   
   return 1;
}

void DisplayColor(int clr, int x, int y)
{
   if (clr == BLACK)
      printf("0 0 0");
   else if (clr == RED)
      printf("255 0 0");
   else if (clr == GREEN)
      printf("0 255 0");
   else if (clr == BLUE)
      printf("0 0 255");
   else if (clr == CYAN)
      printf("0 255 255");
   else if (clr == MAGENTA)
      printf("255 0 255");
   else if (clr == YELLOW)
      printf("255 255 0");
   else if (clr == WHITE)
      printf("255 255 255");
         
   printf(" # %d,%d\n", x, y);
}

int IsGoodColor(int clr)
{
   return !(clr < 0 || clr > 7);
}

int DiffPoint(int x, int offs)
{
   return x -= offs;
}

double Distance(int x1, int y1, int x2, int y2)
{
   return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
}
