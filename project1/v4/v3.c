/*                               80 col header                                */
#include <stdio.h>
#include <assert.h>
#include <math.h>

#define SHAPES 100
#define MAX_SIZE 200

#define INIT_ARGS 3
#define MIN_ARGS 4
#define RECT_ARGS 6
#define ELPS_ARGS 7
#define CIRC_ARGS 5
#define EXCL_ARGS 6
#define LETR_ARGS 7
#define STROKE_FACTOR 4

#define BLACK 0
#define RED 1
#define GREEN 2
#define BLUE 3
#define CYAN 4
#define MAGENTA 5
#define YELLOW 6
#define WHITE 7

typedef struct {
   char type;
   int color, arg0, arg1, arg2;
} Shape;

typedef struct {
   int x, y;
} Point;

int CheckArgs(Shape s, int numArgs);
int InObject(Shape s, Point p);

int Rectangle(Shape s, Point p);
int CheckRectangle(Shape s, int numArgs);

int Ellipse(Shape s, Point p);
int CheckEllipse(Shape s, int numArgs);

int Circle(Shape s, Point p);
int CheckCircle(Shape s, int numArgs);

int Exclaim(Shape s, Point p);
int CheckExclaim(Shape s, int numArgs);

int LetterH(Shape s, Point p);
int LetterL(Shape s, Point p);
int CheckLetter(Shape s, int numArgs);

Point DiffPoint(Point p, int xOffs, int yOffs);
double Distance(Point p1, Point p2);

void DisplayColor(int clr, Point loc);
int IsGoodColor(int clr);

const Point origin = {0, 0};
const int black = 0;

int main(void)
{
   int numArgs, numShapes, imgW, imgH, i, j, k, layer = -1;
   Shape s[SHAPES];
   Point p[SHAPES];
   Point loc;
   
   numArgs = scanf("%d %d %d", &numShapes, &imgW, &imgH);
   
   if (numArgs == EOF) {
      printf ("Empty input file.\n");
      return 0;
   }
   
   else if (numArgs != INIT_ARGS) {
      printf("Initial conditions missing.\n");
      return 0;
   }
   
   if (numShapes < 0 || numShapes > SHAPES) {
      printf("Must have positive number of objects at most %d.\n", SHAPES);
      return 0;
   }
   
   if (imgW < 0 || imgW > MAX_SIZE || imgH < 0 || imgH > MAX_SIZE) {
      printf("Must have positive dimensions at most %d.\n", MAX_SIZE);
      return 0;
   }
   
   for (i = 0; i < numShapes; i++) {
      numArgs = scanf(" %c %d %d %d %d %d %d", &s[i].type, &s[i].color,
       &p[i].x, &p[i].y, &s[i].arg0, &s[i].arg1, &s[i].arg2);
      
      if (numArgs == EOF) {
         printf("Must have %d shapes.\n", numShapes);
         return 0;
      }
      
      if (!CheckArgs(s[i], numArgs)) {
         printf("Error in object %d.\n", i);
         return 0;
      }
   }
   
   numArgs = scanf(" %c %d %d %d %d %d %d", &s[i].type, &s[i].color,
    &p[i].x, &p[i].y, &s[i].arg0, &s[i].arg1, &s[i].arg2);
   
   if (numArgs != EOF) {
      printf("Must have %d shapes.\n", numShapes);
      return 0;
   }
   
   printf("P3 %d %d 255\n", imgW, imgH);
   
   for (i = 0; i < imgH; i++) {
      for (j = 0; j < imgW; j++) {
         loc.x = j;
         loc.y = i;
         
         for (k = numShapes-1; k >= 0; k--)
            if (InObject(s[k], DiffPoint(loc, p[k].x, p[k].y)))
               layer = k;
         
         if (layer != -1)
            DisplayColor(s[layer].color, loc);
         else
            DisplayColor(black, loc);
         
         layer = -1;
      }
   }
   
   return 0;
}

int CheckArgs(Shape s, int numArgs)
{
   if (numArgs < MIN_ARGS) {
      printf("Bad formatting in first %d args.\n", MIN_ARGS);
      return 0;
   }
   
   if (!IsGoodColor(s.color)) {
      printf("Bad color.\n");
      return 0;
   }
   
   if (s.type == 'R')
      return CheckRectangle(s, numArgs);
   else if (s.type == 'E')
      return CheckEllipse(s, numArgs);
   else if (s.type == 'C')
      return CheckCircle(s, numArgs);
   else if (s.type == '!')
      return CheckExclaim(s, numArgs);
   else if (s.type == 'H' || s.type == 'L')
      return CheckLetter(s, numArgs);
   else {
      printf("Bad shape %c.\n", s.type);
      return 0;
   }
   
   return 1;
}

int InObject(Shape s, Point p)
{
   if (s.type == 'R')
      return Rectangle(s, p);
   else if (s.type == 'C')
      return Circle(s, p);
   else if (s.type == 'E')
      return Ellipse(s, p);
   else if (s.type == '!')
      return Exclaim(s, p);
   else if (s.type == 'H')
      return LetterH(s, p);
   else if (s.type == 'L')
      return LetterL(s, p);
   else
      return 0;
}

int Rectangle(Shape s, Point p)
{
   return !(p.x < 0 || p.x >= s.arg0 || p.y < 0 || p.y >= s.arg1);
}

int CheckRectangle(Shape s, int numArgs)
{
   if (numArgs != RECT_ARGS) {
      printf("Rectangle must have exactly two arguments.\n");
      return 0;
   }
   
   if (s.arg0 < 0 || s.arg1 < 0) {
      printf("Rectangle must have positive width and height.\n");
      return 0;
   }
   
   return 1;
}

int Ellipse(Shape s, Point p)
{
   Point focus = {s.arg0, s.arg1};
   return ((Distance(p, origin) + Distance(p, focus)) < s.arg2);
}

int CheckEllipse(Shape s, int numArgs)
{
   if (numArgs != ELPS_ARGS) {
      printf("Ellipse must have exactly three arguments.\n");
      return 0;
   }
   
   if (s.arg2 < 0) {
      printf("Ellipse must have positive axis.\n");
      return 0;
   }
   
   return 1;
}

int Circle(Shape s, Point p)
{
   return (Distance(origin, p) < s.arg0);
}

int CheckCircle(Shape s, int numArgs)
{
   if (numArgs != CIRC_ARGS) {
      printf("Circle must have exactly one argument.\n");
      return 0;
   }
   
   if (s.arg0 < 0) {
      printf("Circle must have positive radius.\n");
      return 0;
   }
   
   return 1;
}

int Exclaim(Shape s, Point p)
{
   Shape eShape;
   Point ePoint = {p.x, p.y - s.arg1 - s.arg0 / 2};
   
   eShape.arg0 = s.arg0;
   eShape.arg1 = s.arg0;
   
   return (Rectangle(s, p) || Rectangle(eShape, ePoint));
}

int CheckExclaim(Shape s, int numArgs)
{
   if (numArgs != EXCL_ARGS) {
      printf("Exclamation must have exactly two arguments.\n");
      return 0;
   }
   
   if (s.arg0 < 0 || s.arg1 < 0) {
      printf("Exclamation must have positive width and height.\n");
      return 0;
   }
   
   return 1;
}

int LetterH(Shape s, Point p)
{
   Shape leftS, rightS, midS;
   Point rightP = {s.arg0 - s.arg2, 0}, midP = {0, s.arg1 / 2};
   
   leftS.arg0 = s.arg2;
   rightS.arg0 = s.arg2;
   midS.arg1 = s.arg2;
   
   if (Rectangle(leftS, p) || Rectangle(rightS, rightP) ||
    Rectangle(midS, midP))
      return 1;
   return 0;
}

int LetterL(Shape s, Point p)
{
   Shape leftS, botS;
   Point botP = {0, s.arg1 - s.arg2};
   
   leftS.arg0 = s.arg2;
   botS.arg1 = s.arg2;
   
   return (Rectangle(leftS, p) || Rectangle(botS, botP));
}

int CheckLetter(Shape s, int numArgs)
{
   if (numArgs != LETR_ARGS) {
      printf("Letters must have exactly three arguments.\n");
      return 0;
   }
   
   if (s.arg0 < 0 || s.arg1 < 0) {
      printf("Letters must have positive width and height.\n");
      return 0;
   }
   
   if (s.arg2 * STROKE_FACTOR > s.arg0 || s.arg2 * STROKE_FACTOR > s.arg1) {
      printf("Letter %c must have width and height at least %d*stroke.\n",
       s.type, STROKE_FACTOR);
      return 0;
   }
   
   return 1;
}

Point DiffPoint(Point p, int xOffs, int yOffs)
{
   p.x -= xOffs;
   p.y -= yOffs;
   return p;
}

double Distance(Point p1, Point p2)
{
   return sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
}

void DisplayColor(int clr, Point loc)
{
   if (clr == BLACK)
      printf("0 0 0");
   else if (clr == RED)
      printf("255 0 0");
   else if (clr == GREEN)
      printf("0 255 0");
   else if (clr == BLUE)
      printf("0 0 255");
   else if (clr == CYAN)
      printf("0 255 255");
   else if (clr == MAGENTA)
      printf("255 0 255");
   else if (clr == YELLOW)
      printf("255 255 0");
   else if (clr == WHITE)
      printf("255 255 255");
         
   printf(" # %d,%d\n", loc.x, loc.y);
}

int IsGoodColor(int clr)
{
   return !(clr < 0 || clr > 7);
}

