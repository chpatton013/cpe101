#ifndef POINT_H
#define POINT_H

/* A struct representing a single x/y point. */
typedef struct {
   int x;
   int y;
} Point;

// Return a new point, obtained by subtracting xOffs and yOffs from the x and
// y dimensions of p.  In effect, return a point that is p's relative offset
// from the point (xOffs, yOffs).  This method may be useful when you need
// to treat (xOffs, yOffs) as if it were the (0,0) origin, and you want to
// adjust points accordingly.
Point DiffPoint(Point p, int xOffs, int yOffs);

// Return the distance between Points p1 and p2
double Distance(Point p1, Point p2);

#endif
