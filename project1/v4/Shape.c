/*                               80 col header                                */
#include <stdio.h>
#include <assert.h>

#include "Shape.h"
#include "Point.h"

#define RECT_ARGS 8
#define ELPS_ARGS 9
#define CIRC_ARGS 7
#define EXCL_ARGS 8
#define LETR_ARGS 9
#define STROKE_FACTOR 4

const Point origin = {0, 0};

int Rectangle(Shape s, Point p)
{
   return !(p.x < 0 || p.x >= s.arg0 || p.y < 0 || p.y >= s.arg1);
}

int CheckRectangle(Shape s, int numArgs)
{
   if (numArgs != RECT_ARGS) {
      printf("Rectangle must have exactly two arguments.\n");
      return 0;
   }
   
   if (s.arg0 < 0 || s.arg1 < 0) {
      printf("Rectangle must have positive width and height.\n");
      return 0;
   }
   
   return 1;
}

int Ellipse(Shape s, Point p)
{
   Point focus = {s.arg0, s.arg1};
   return ((Distance(p, origin) + Distance(p, focus)) < s.arg2);
}

int CheckEllipse(Shape s, int numArgs)
{
   if (numArgs != ELPS_ARGS) {
      printf("Ellipse must have exactly three arguments.\n");
      return 0;
   }
   
   if (s.arg2 < 0) {
      printf("Ellipse must have positive axis.\n");
      return 0;
   }
   
   return 1;
}

int Circle(Shape s, Point p)
{
   return (Distance(origin, p) < s.arg0);
}

int CheckCircle(Shape s, int numArgs)
{
   if (numArgs != CIRC_ARGS) {
      printf("Circle must have exactly one argument.\n");
      return 0;
   }
   
   if (s.arg0 < 0) {
      printf("Circle must have positive radius.\n");
      return 0;
   }
   
   return 1;
}

int Exclaim(Shape s, Point p)
{
   Shape eShape;
   Point ePoint = {p.x, p.y - s.arg1 - s.arg0 / 2};
   
   eShape.arg0 = s.arg0;
   eShape.arg1 = s.arg0;
   
   return (Rectangle(s, p) || Rectangle(eShape, ePoint));
}

int CheckExclaim(Shape s, int numArgs)
{
   if (numArgs != EXCL_ARGS) {
      printf("Exclamation must have exactly two arguments.\n");
      return 0;
   }
   
   if (s.arg0 < 0 || s.arg1 < 0) {
      printf("Exclamation must have positive width and height.\n");
      return 0;
   }
   
   return 1;
}

int LetterH(Shape s, Point p)
{
   Shape leftS, rightS, midS;
   Point rightP = {s.arg0 - s.arg2, 0}, midP = {0, s.arg1 / 2};
   
   leftS.arg0 = s.arg2;
   rightS.arg0 = s.arg2;
   midS.arg1 = s.arg2;
   
   if (Rectangle(leftS, p) || Rectangle(rightS, rightP) ||
    Rectangle(midS, midP))
      return 1;
   return 0;
}

int LetterL(Shape s, Point p)
{
   Shape leftS, botS;
   Point botP = {0, s.arg1 - s.arg2};
   
   leftS.arg0 = s.arg2;
   botS.arg1 = s.arg2;
   
   return (Rectangle(leftS, p) || Rectangle(botS, botP));
}

int CheckLetter(Shape s, int numArgs)
{
   if (numArgs != LETR_ARGS) {
      printf("Letters must have exactly three arguments.\n");
      return 0;
   }
   
   if (s.arg0 < 0 || s.arg1 < 0) {
      printf("Letters must have positive width and height.\n");
      return 0;
   }
   
   if (s.arg2 * STROKE_FACTOR > s.arg0 || s.arg2 * STROKE_FACTOR > s.arg1) {
      printf("Letter %c must have width and height at least %d*stroke.\n",
       s.type, STROKE_FACTOR);
      return 0;
   }
   
   return 1;
}
