/*                               80 col header                                */
#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "Shape.h"
#include "Color.h"
#include "Point.h"

#define SHAPES 100
#define MAX_SIZE 200
#define INIT_ARGS 3
#define MIN_ARGS 6

int CheckArgs(Shape s, int numArgs);
int InObject(Shape s, Point p);

int main(void)
{
   int numArgs, numShapes, imgW, imgH, i, j, k, layer = -1;
   Shape s[SHAPES];
   Point p[SHAPES];
   Point loc;
   Color black = {0, 0, 0};
   
   numArgs = scanf("%d %d %d", &numShapes, &imgW, &imgH);
   
   if (numArgs == EOF) {
      printf ("Empty input file.\n");
      return 0;
   }
   
   else if (numArgs != INIT_ARGS) {
      printf("Initial conditions missing.\n");
      return 0;
   }
   
   if (numShapes < 0 || numShapes > SHAPES) {
      printf("Must have positive number of objects at most %d.\n", SHAPES);
      return 0;
   }
   
   if (imgW < 0 || imgW > MAX_SIZE || imgH < 0 || imgH > MAX_SIZE) {
      printf("Must have positive dimensions at most %d.\n", MAX_SIZE);
      return 0;
   }
   
   for (i = 0; i < numShapes; i++) {
      numArgs = scanf(" %c %d %d %d %d %d %d %d %d", &s[i].type,
       &s[i].color.red, &s[i].color.green, &s[i].color.blue,
       &p[i].x, &p[i].y, &s[i].arg0, &s[i].arg1, &s[i].arg2);
      
      if (numArgs == EOF) {
         printf("Must have %d shapes.\n", numShapes);
         return 0;
      }
      
      if (!CheckArgs(s[i], numArgs)) {
         printf("Error in object %d.\n", i);
         return 0;
      }
   }
   
   numArgs = scanf(" %c %d %d %d %d %d %d %d %d", &s[i].type,
    &s[i].color.red, &s[i].color.green, &s[i].color.blue,
    &p[i].x, &p[i].y, &s[i].arg0, &s[i].arg1, &s[i].arg2);
   
   if (numArgs != EOF) {
      printf("Must have %d shapes.\n", numShapes);
      return 0;
   }
   
   printf("P3 %d %d 255\n", imgW, imgH);
   
   for (i = 0; i < imgH; i++) {
      for (j = 0; j < imgW; j++) {
         loc.x = j;
         loc.y = i;
         
         for (k = numShapes-1; k >= 0; k--)
            if (InObject(s[k], DiffPoint(loc, p[k].x, p[k].y)))
               layer = k;
         
         if (layer != -1)
            DisplayColor(s[layer].color, loc);
         else
            DisplayColor(black, loc);
         
         layer = -1;
      }
   }
   
   return 0;
}

int CheckArgs(Shape s, int numArgs)
{
   if (numArgs < MIN_ARGS) {
      printf("Bad formatting in first %d args.\n", MIN_ARGS);
      return 0;
   }
   
   if (!IsGoodColor(s.color)) {
      printf("Bad color.\n");
      return 0;
   }
   
   if (s.type == 'R')
      return CheckRectangle(s, numArgs);
   else if (s.type == 'E')
      return CheckEllipse(s, numArgs);
   else if (s.type == 'C')
      return CheckCircle(s, numArgs);
   else if (s.type == '!')
      return CheckExclaim(s, numArgs);
   else if (s.type == 'H' || s.type == 'L')
      return CheckLetter(s, numArgs);
   else {
      printf("Bad shape %c.\n", s.type);
      return 0;
   }
   
   return 1;
}

int InObject(Shape s, Point p)
{
   if (s.type == 'R')
      return Rectangle(s, p);
   else if (s.type == 'C')
      return Circle(s, p);
   else if (s.type == 'E')
      return Ellipse(s, p);
   else if (s.type == '!')
      return Exclaim(s, p);
   else if (s.type == 'H')
      return LetterH(s, p);
   else if (s.type == 'L')
      return LetterL(s, p);
   else
      return 0;
}
