#include <stdio.h>

int main(void)
{
   int bottle;

   for (bottle = 99; bottle > 0; bottle--) {
      printf("%d bottles of beer on the wall,\n", bottle);
      printf("%d bottles of beer.\n", bottle);
      printf("Take one down, pass it around.\n", bottle);
      printf("%d bottles of beer on the wall.\n\n", bottle-1);
   }
   return 0;
}
