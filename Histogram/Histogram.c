#include <stdio.h>

int main()
{
   int i, temp = 0, arr[10];

   for (i = 0; i < 10; i++) {
      arr[i] = 0;
   }

   while (i != EOF) {
      i = scanf("%d", &temp);

      if (temp < 10 && temp >= 0 && i != EOF)
         arr[temp]++;
   }

   for (i = 0; i < 10; i++)
      printf("%d %d's\n", arr[i], i);

   return 0;
}