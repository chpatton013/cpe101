#include <stdio.h>

int main(void)
{
   int num, i, j, k;
   
   printf("Enter an odd positive integer: ");
   scanf("%d", &num);
   
   while (num < 1 || num%2 == 0) {
      printf("Enter an odd positive integer: ");
      scanf("%d", &num);
   }
   
   printf("\n");
      
   for (i = 0; i < num / 2 + 1; i++) {
      for (j = 0; j < i*2; j++) {
         printf(" ");
      }
      for (k = 0; k < num-j; k++) {
         printf("X");
      }
      printf("\n");
   }
   
   return 0;
}
