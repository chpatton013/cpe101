#include <stdio.h>

int main(void)
{
   int pos = 0, neg = 0, flag = -1;
   
   while (flag != 0) {
      printf("Enter an int: ");
      scanf("%d", &flag);
      
      if (flag > 0)
         pos += flag;
      else if (flag < 0)
         neg += flag;
   }

   printf("Total of positives is: %d\n", pos);
   printf("Total of negatives is: %d\n", neg);

   return 0;
}
