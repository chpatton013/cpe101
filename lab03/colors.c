#include <stdio.h>
#define ROW 20  /* change number of rows */
#define COL 20  /* change number of columns */

int main(void) {
  char colors[ROW][COL], color='0';
  int row,col;
  
  /* clear array */
  for (row=0; row<ROW; row++) for (col=0; col<COL; col++) 
    colors[row][col]='0';
  
  /* fill array */
  for (row=0; row<ROW; row++) {
    for (col=0; col<COL; col++) {
      color='0';
      
      /* start test code */



      /* end test code */
      
      colors[row][col]=color;
    }
  }
  
  /* print array */
  for (row=0; row<ROW; row++) {
    for (col=0; col<COL; col++) {
      printf(" %c", colors[row][col]);
    }
    printf("\n");
  }

  return 0;
}
