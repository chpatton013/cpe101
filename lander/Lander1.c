#include <stdio.h>
#define GRAVITY 1.6

int main() {
   double height=1000.0,
          speed=100.0,
          newSpeed,
          burnRate,
          fuelLeft=200.0;


   while (height>0.0) {
      printf("You are at %0.3f m, falling at %0.3f m/s", height, speed);
      printf(" with %0.3f kg of fuel.\n", fuelLeft);
      printf("What burn rate do you want? (0 to 10) ");
      scanf("%lf", &burnRate);
      
      while (burnRate>10 || burnRate<0) {
         printf("Burn rate must be between 0 and 10, inclusive\n");
         printf("What burn rate do you want? (0 to 10) ");
         scanf("%lf", &burnRate);
      }
      
      newSpeed = speed + GRAVITY - burnRate;
      fuelLeft = fuelLeft - burnRate;
      height = height - (speed + newSpeed)/2;
      speed = newSpeed;
   }
   
   return 0;
}
