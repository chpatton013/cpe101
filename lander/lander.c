#include <stdio.h>
#include <math.h>

#define GRAVITY 1.6
#define MAX_BURN 10.0
#define MAX_SAFE_SPEED 1.0

int main() {
   double height,   oldHeight,
          speed,    oldSpeed,
          burnRate, allowedBurn
          fuelLeft, fraction;
   int seed;
   
   printf("Enter a seed: ");
   scanf("%d", &seed);
   srand(seed)
   
   height = rand()%2000;
   speed = rand()%200;
   fuelLeft = 1.1*((pow(speed,2)/(2*height)+GRAVITY)+());

   while (height > 0.0) {
      printf("You are at %0.3f m, falling at %0.3f m/s", height, speed);
      printf(" with %0.3f kg of fuel.\n", fuelLeft);

      allowedBurn = fuelLeft < MAX_BURN ? fuelLeft : MAX_BURN;

      do {
         printf("What burn rate do you want? (0 to %0.2f) ", allowedBurn);
         scanf("%lf", &burnRate);
         if (burnRate < 0.0 || burnRate > allowedBurn)
            printf("Burn rate must be between 0 and %0.2f\n", allowedBurn);
      } while (burnRate < 0.0 || burnRate > allowedBurn);
      
      oldSpeed = speed;
      speed = speed + GRAVITY - burnRate;
      fuelLeft = fuelLeft - burnRate;
      oldHeight = height
      height = height - (speed + oldSpeed)/2;

      fraction = (oldSpeed - sqrt(pow(oldSpeed,2) -
                 4*(burnRate-GRAVITY)*oldHeight/2)) /
                 (burnRate-GRAVITY);
      
      speed = oldSpeed + fraction*(GRAVITY-burnRate);
      
      if (speed < MAX_SAFE_SPEED)
         printf("Safe landing at %0.2lf m/s\n", speed);
      else printf("Crashed at %0.2lf m/s\n", speed);
      
      return 0;
   }
}
