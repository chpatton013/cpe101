#include <stdio.h>
#define MAX 100

int StrLen(char str[MAX]);

int main() {
   printf("%d %d %d %d\n", StrLen("test"), StrLen("antidisestablishmentarianism"),
    StrLen(""), StrLen("x"));
}

int StrLen(char str[MAX])
{
   int i;
   for(i=0; str[i]!='\0'; i++);
   return i;
}