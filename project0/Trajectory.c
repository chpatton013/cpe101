#include<stdio.h>
#include<math.h>

#define G 9.8
#define PI (3.141592653589793)
#define V_PER_KG 100.0
#define RAD_DEG PI/180.0
#define MAX_A 90
#define MIN_A 0
#define HIT_RANGE 1.0
#define MAX_DIST 1000

int checkValid(double powder, double angle);
int checkHit(double dist, double powder, double angle);

int main(void)
{
   int seed, flag=0;
   double dist, powder, angle;
   
   printf("Enter a seed value greater than 0: ");
   scanf(" %d", &seed);
   
   while (seed<=0) {
      printf("%d is not greater than 0.\n", seed);
      printf("Enter a seed value greater than 0: ");
      scanf(" %d", &seed);
   }
   
   srand(seed);
   dist = rand()%MAX_DIST;
   
   printf("Target is at distance %0.1lf.\n", dist);
   
   while (!flag) {
      printf("Enter amount of powder and angle of elevation: ");
      scanf(" %lf %lf", &powder, &angle);
      if (checkValid(powder,angle)) flag=checkHit(dist,powder,angle);
   }
    
   return 0;
}

int checkValid(double powder, double angle)
{
   int check=1;
   
   if (powder<0) {
      printf("Enter a nonnegative amount of powder.\n");
      check=0;
   }

   if (!(angle>=MIN_A && angle<=MAX_A)) {
      printf("Enter an angle between 0 and 90, inclusive.\n");
      check=0;
   }
   
   return check;
}

int checkHit(double dist, double powder, double angle)
{
   double vel, velx, vely, range;
   
   vel = powder*V_PER_KG;
   velx = cos(angle*RAD_DEG)*vel;
   vely = sin(angle*RAD_DEG)*vel;
   range = velx*(2.0*vely/G);
   
   if (range-dist>HIT_RANGE) {
      printf("Long by %0.2lf. Try again.\n", range-dist);
      return 0;
   }
   
   else if (dist-range>HIT_RANGE) {
      printf ("Short by %0.2lf. Try again.\n", dist-range);
      return 0;
   }
   
   else {
      printf("Hit!\n");
      return 1;
 
  }
}
