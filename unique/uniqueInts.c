#include <stdio.h>

#define MAXIN 100

int main()
{
   int arr[MAXIN], i, ndx = 0, temp, flag = 1;

   while (scanf("%d", &temp) != EOF) {

      for (i = 0; i < ndx; i++)
         if (temp == arr[i])
            flag = 0;

      if (flag) {
         arr[ndx] = temp;
         printf("%d ", arr[ndx]);
      }

      flag = 1;
      ndx++;
   }

   return 0;
}