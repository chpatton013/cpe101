#include <stdio.h>
#include <math.h>

#define MAXIN 100

typedef struct {
   double x, y;
} Point;

typedef struct {
   Point p;
   double r;
} Circle;

int overlap(Circle c1, Circle c2);
double distance(Point p1, Point p2);

int main()
{
   int i, ndx = 0, flag = 1;
   Circle arr[MAXIN], temp;

   while (scanf("%lf %lf %lf", &temp.p.x, &temp.p.y, &temp.r) != EOF) {

      for (i = 0; i < ndx; i++)
         if (overlap(arr[i], temp))
            flag = 0;

      if (flag) {
         arr[ndx] = temp;
         printf("%.2f %.2f %.2f\n", arr[ndx].p.x, arr[ndx].p.y, arr[ndx].r);
      }

      flag = 1;
      ndx++;
   }

   return 0;
}

int overlap(Circle c1, Circle c2)
{
   return (distance(c1.p, c2.p) < c1.r + c2.r);
}

double distance(Point p1, Point p2)
{
   return sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
}