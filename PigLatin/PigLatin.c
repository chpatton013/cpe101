#include <stdio.h>
#include <string.h>
#include <ctype.h>

void printVowel(char ch) {
   do {
      putchar(ch);
   } while (ch = getchar(), isalpha(ch));

   printf("hay%c", ch);
}

void printConsonant(char ch) {
   char first = ch;

   while (ch = getchar(), isalpha(ch))
      putchar(ch);

   printf("%cay%c", first, ch);
}

void main(void) {
   char ch;

   while (ch = getchar(), ch != EOF) {
      isalpha(ch) ?
         strchr("AEIOUaeiou", ch) ? printVowel(ch) : printConsonant(ch) :
         putchar(ch);
   }
}
