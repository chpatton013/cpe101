#include <unistd.h>
#include <stdlib.h>
#include <libgen.h>
#include <string.h>

#define PATH_LEN 500

void main(int argc, char *argv[])
{
   char direc[PATH_LEN];
   char base[PATH_LEN];

   strcpy(direc, dirname(getenv("PWD")));
   strcpy(base, basename(direc));
   strcat(strcat(direc, "/"), base);

   execvp(direc, argv);
}
